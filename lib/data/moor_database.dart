import 'package:moor/moor.dart';
import 'package:moor_flutter/moor_flutter.dart';

part 'moor_database.g.dart';

// Database title will be Tasks
// The name of generated data class will be task
// Default data class name Task will now be Sleepy
//@DataClassName('Task')
class Tasks extends Table {
  // Custom primary key define
  @override
  // autoIncrement set the id to the primary key
  IntColumn get id => integer().autoIncrement()();

  // If length is not constraint, Task will be inserted into database
  // Exception will be thrown
  TextColumn get name => text().withLength(min: 1, max: 50)();

  // DateTime is not natively support by SQLite
  // That why cant name it to dateTime
  // Moor converts it to & from UNIX seconds
  DateTimeColumn get dueDate => dateTime().nullable()();

  // Booleans are not supported as well. Moor converts them to integer
  // Simple default value specified as Constant
  BoolColumn get completed => boolean().withDefault(Constant(false))();

// Custom primary keys defined as a set of column
  @override
  Set <Column> get primaryKey => {id, name};
}

// This annotation tells which tables this dB work with
@UseMoor(tables: [Tasks])

// _$AppDatabase is the name of the generated class
class AppDatabase extends _$AppDatabase {
  AppDatabase()
  // This specified location of database file
      : super(FlutterQueryExecutor.inDatabaseFolder(
            path: 'db.sqlite',
      // this log statement use for debugging - print SQL in console
      logStatements: true));

  // Do this for changing tables and column
  // Its important for migration in the next part
  @override
  // TODO: implement schemaVersion
  int get schemaVersion => 1;

  // All tables have getter method in the generated class
  // We make this to select it from task table
  Future<List<Task>> getAllTasks() => select(tasks).get();

  // Moor also support Stream to emit element when data changes
  Stream<List<Task>> watchAllTasks() => select(tasks).watch();

  Future insertTask(Task task) => into(tasks).insert(task);

  // Update task with matching primary key
  Future updateTask(Task task) => update(tasks).replace(task);

  Future deleteTask(Task task) => delete(tasks).delete(task);
}
